//
//  LineManApp.swift
//  LineMan
//
//  Created by admin on 4/5/24.
//

import SwiftUI
import SDWebImageSwiftUI
import SDWebImageSVGCoder

@main
struct LineManApp: App {
    init() {
        self.setUpDependencies()        
    }
    
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}

extension LineManApp {
    func setUpDependencies() {
        SDImageCodersManager.shared.addCoder(SDImageSVGCoder.shared)
    }
}
